import QtQuick 2.3
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import com.people 1.0

Window {
    id: rootWindow
    visible: true
    x: Screen.width / 2 - width / 2
    y: Screen.height / 2 - height / 2
    width: 640
    height: 520

    PeopleList {
        id: peopleList
    }

    Rectangle {
        color: "#2a2d33"
        border.color: "black"
        border.width: 1
        anchors.fill: parent

        Text {
            id: logo
            text: qsTr("PatternAnalysis")
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 30
            anchors.leftMargin: 20
            color: "#ffffff"
            font.bold: true
            font.pixelSize: 28
            x: 10
            y: 0

        }


        Rectangle {
          id: list
          height: 200
          anchors.left: parent.left
          anchors.top: logo.bottom
          width: parent.width / 4
          anchors.rightMargin: 10
          anchors.leftMargin: 10
          anchors.topMargin: 20


          ListModel {
            id: segmentModel
            ListElement { text: 'X-Y Segmentation' }
            ListElement { text: 'Unite X-Y' }
            ListElement { text: 'Clean from fake I' }
            ListElement { text: 'V Segmrntation' }
            ListElement { text: 'Unite V' }
          }

          ListView {
            id: segment
            anchors.fill: parent
            model: segmentModel
            focus: true
            highlightFollowsCurrentItem: true

            delegate: Rectangle {
              width: segment.width
              height: 40
              border {
                  color: "black"
                  width: 1
              }
              property var view: ListView.view
              property var isCurrent: ListView.isCurrentItem

              Text {
                  anchors.centerIn: parent
                  renderType: Text.NativeRendering
                  text: model.text
              }
              MouseArea {
                  anchors.fill: parent
                  onClicked: {
                    segment.currentIndex = index
                    console.log(segmentModel.get(segment.currentIndex).text)
                    //parent.color = '#6F7071'
                  }
              }
            }
            highlight: Rectangle {
              color: 'grey'
              Text {
                  anchors.centerIn: parent
                  text: segmentModel.get(segment.currentIndex).text
                  color: 'white'
              }
            }
          }
        }

        Rectangle {
          id: prop
          height: 200
          anchors.left: list.right
          anchors.top: logo.bottom
          anchors.right: parent.right
          anchors.bottom: makeStepOrRun.top
          anchors.rightMargin: 10
          anchors.leftMargin: 10
          anchors.topMargin: 20

          TableView {
            id: generalTable

            frameVisible: true
            sortIndicatorVisible: true

            anchors.fill: parent

            TableViewColumn {
              role: "Prop"
              title: "Свойства"
            }

            TableViewColumn {
              role: "XY"
              title: "X-Y Segmentation"
            }

            TableViewColumn {
              role: "Aggreg"
              title: "Aggregation"
            }

            TableViewColumn {
              role: "Quantille based"
              title: "Quantille based"
            }

            TableViewColumn {
              role: "V segmentation"
              title: "V segmentation"
            }

            TableViewColumn {
              role: "VAggreg"
              title: "Aggregation"
            }

            model: propertyModel

          }
        }

        Button {
            id: makeStepOrRun
            text: "Run"
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.bottomMargin : 15
            height: 35
            width: 150

            onClicked: {
              console.log("!" + peopleList.makeSegments());
              generalTable.model.update();
            }


        }

    }

  
}