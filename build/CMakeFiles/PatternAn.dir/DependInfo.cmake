# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tt/ClionProjects/PatternAn/build/PatternAn_automoc.cpp" "/home/tt/ClionProjects/PatternAn/build/CMakeFiles/PatternAn.dir/PatternAn_automoc.cpp.o"
  "/home/tt/ClionProjects/PatternAn/Source/Person.cpp" "/home/tt/ClionProjects/PatternAn/build/CMakeFiles/PatternAn.dir/Source/Person.cpp.o"
  "/home/tt/ClionProjects/PatternAn/Source/Segment.cpp" "/home/tt/ClionProjects/PatternAn/build/CMakeFiles/PatternAn.dir/Source/Segment.cpp.o"
  "/home/tt/ClionProjects/PatternAn/Source/Statistics.cpp" "/home/tt/ClionProjects/PatternAn/build/CMakeFiles/PatternAn.dir/Source/Statistics.cpp.o"
  "/home/tt/ClionProjects/PatternAn/Source/driveWorker.cpp" "/home/tt/ClionProjects/PatternAn/build/CMakeFiles/PatternAn.dir/Source/driveWorker.cpp.o"
  "/home/tt/ClionProjects/PatternAn/main.cpp" "/home/tt/ClionProjects/PatternAn/build/CMakeFiles/PatternAn.dir/main.cpp.o"
  "/home/tt/ClionProjects/PatternAn/build/qrc_qml.cpp" "/home/tt/ClionProjects/PatternAn/build/CMakeFiles/PatternAn.dir/qrc_qml.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "../Resources,"
  "../Source"
  "/home/tt/Qt5.7.0/5.7/gcc_64/include"
  "/home/tt/Qt5.7.0/5.7/gcc_64/include/QtCore"
  "/home/tt/Qt5.7.0/5.7/gcc_64/./mkspecs/linux-g++"
  "/home/tt/Qt5.7.0/5.7/gcc_64/include/QtWidgets"
  "/home/tt/Qt5.7.0/5.7/gcc_64/include/QtGui"
  "/home/tt/Qt5.7.0/5.7/gcc_64/include/QtQuick"
  "/home/tt/Qt5.7.0/5.7/gcc_64/include/QtQml"
  "/home/tt/Qt5.7.0/5.7/gcc_64/include/QtNetwork"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
