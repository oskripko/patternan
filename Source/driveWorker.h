#ifndef PATTERNAN_DRIVEWORKER_H
#define PATTERNAN_DRIVEWORKER_H

#include <QObject>
#include <QtCore/QStringList>
#include <QtCore/QString>
#include <QAbstractTableModel>
#include <vector>

#include "Dot.h"
#include "Person.h"



class driveWorker {
//Подгрузка-выгрузка
public:
    QStringList getFileNames();
	PersonProp readCSV();
	static void writeToFile(const std::vector<Dot> &dots);
	PersonProp *prop = new PersonProp();
private:

};

struct Property {
	std::vector<Dot> segments;
	int nDot = 0;
	int nD = 0;
	int nI = 0;
	int nB = 0;
	double countMeanDDur = 0;
	double countMeanIDur = 0;
	double countMeanBDur = 0;
	double frequencyB = 0;
	double frequencyD = 0;
	double frequencyI = 0;
	double meanIntervalBetwD = 0;
	double meanIntervalBetwB = 0;
	double meanIntervalBetwI = 0;
};

struct SegTypes {
	Property first;
	Property second;
	Property third;
	Property fourth;
	Property fifth;
};

struct Line {
	Line(QString name, QString first, QString second, QString third, QString fourth, QString fifth) :
		m_name(name), m_first(first), m_second(second), m_third(third), m_fourth(fourth), m_fifth(fifth) {}
	
	QString name() const;
	QString first() const;
	QString second() const;
	QString third() const;
	QString fourth() const;
	QString fifth() const;

	void setName(QString &name) {
		m_name = name;
	}
	void setFirst(QString &fst) {
		m_first = fst;
	}
	void setSecond(QString &snd) {
		m_second = snd;
	}
	void setThird(QString &trd) {
		m_third = trd;
	}
	void setFourth(QString &frt) {
		m_fourth = frt;
	}
	void setFifth(QString &fft) {
		m_fifth = fft;
	}
private:
	QString m_name;
	QString m_first;
	QString m_second;
	QString m_third;
	QString m_fourth;
	QString m_fifth;
};


class PropertyModel : public QAbstractTableModel {
	Q_OBJECT
public:
	PropertyModel(QObject *parent = 0);

	void addLine(const Line &line);

	int rowCount(const QModelIndex & parent = QModelIndex()) const {
		return 13;
	}

	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

	int columnCount(const QModelIndex &parent) const {
		return 6;
	}

	bool setData(const QModelIndex &index, const QVariant &value, int role) override {
		switch (role) {
		case 0: m_lines[index.row()].setName(value.toString()); break;
		case 1: m_lines[index.row()].setFirst(value.toString()); break;
		case 2: m_lines[index.row()].setSecond(value.toString()); break;
		case 3: m_lines[index.row()].setThird(value.toString()); break;
		case 4: m_lines[index.row()].setFourth(value.toString()); break;
		case 5: m_lines[index.row()].setFifth(value.toString()); break;
		}
		dataChanged(index, index);

		return true;
	}

	Qt::ItemFlags flags(const QModelIndex &index) const {
		return Qt::ItemIsEditable;
	}

	Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE {
		Q_UNUSED(parent);
		beginRemoveRows(QModelIndex(), row, row + count - 1);
		while (count--) m_lines.removeAt(row);
		endRemoveRows();
		return true;
	}

	void removeLastLine() {
		auto idx = m_lines.size() - 1;
		beginRemoveRows(QModelIndex(), idx, idx);
		m_lines.removeAt(idx);
		endRemoveColumns();
	}

	QHash<int, QByteArray> roleNames() const;

	Q_INVOKABLE void update() {
		for (int i = 0; i < m_lines.size(); ++i) {
			setData(index(i, 0), m_lines[i].name(), 0);
			setData(index(i, 0), m_lines[i].first(), 1);
			setData(index(i, 0), m_lines[i].second(), 2);
			setData(index(i, 0), m_lines[i].third(), 3);
			setData(index(i, 0), m_lines[i].fourth(), 4);
			setData(index(i, 0), m_lines[i].fifth(), 5);
		}
	}

	void setLineValue(int row, QString &first, QString &second, QString &third, QString &fourth, QString &fifth) {
		m_lines[row].setFirst(first);
		m_lines[row].setSecond(second);
		m_lines[row].setThird(third);
		m_lines[row].setFourth(fourth);
		m_lines[row].setFifth(fifth);
	}


private:
	static QList<Line> m_lines;
};


class PeopleList : public QObject{
    Q_OBJECT
	//Q_PROPERTY(QObject tableModel MEMBER model)
public:
    PeopleList();
	PeopleList(int nDot, int nD, int nI);
	Q_INVOKABLE QString makeSegments();
	QList<QObject*> dotProperties;

	//int gnDot() const;
	//int gnD() const;

	SegTypes segments;
	PropertyModel model;

	void addInitData();

	void gedDataFromVec(std::vector<Dot> vec);

signals:
	void vectorReady(std::vector<Dot> vec);
	void propertyReady();
private:
	Person *person;
	PersonProp *prop = new PersonProp();
	Property setAllValues(std::vector<Dot> vec, Property segs);
	int segNumber = 0;
	
};


#endif //PATTERNAN_DRIVEWORKER_H
