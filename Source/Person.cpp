﻿//
// Created by tt on 8/15/16.
//

#include "Person.h"
#include "driveWorker.h"
#include <iostream>
#include <algorithm>

using namespace std;



Person::Person(PersonProp *prop):quantille(0),property(prop){
	
}

Person::~Person() {
	//Запись векторов (границ) в файл
}


std::vector<Dot> Person::makeSegments() {
	if (coordFstSeg.empty()) {
		return coordFstSeg = initialSeg(true);
	}
	else if (coordSndSeg.empty()) {
		return coordSndSeg = unite(coordFstSeg);

	}
	else if (coordTrdSeg.empty()) {
		getQuantile(coordSndSeg);
		for (auto dot : coordFstSeg) {
			dot.checkType(quantille);
		}
		return coordTrdSeg = unite(coordSndSeg);

	}
	else if (velFstSeg.empty()) {
		getQuantile(coordTrdSeg);
		return velFstSeg = initialSeg(false);
	}
	else if (velSndSeg.empty())
		return velSndSeg = unite(velFstSeg);
	else{
		return std::vector<Dot>();
		//Шота не так
	}
	
}
std::vector<Dot> Person::initialSeg(bool coord)
{
	Dot *dot = new Dot(property->coord.cbegin());
	Point center = *property->coord.cbegin();
	Point newCenter;
	std::vector<Dot> vec;
	int n = 1;
	for (auto a = std::next(property->coord.cbegin()), end = property->coord.cend(); a != end; ++a) {

		if (!a->isBlink) {

			if (!coord)
				dot->setType(Type::I);
			newCenter = getNewCenter(center, n, *a);
			if (!dot->diffIsLess(newCenter, a, coord, quantille)) {
				dot->addLast(a);
				vec.push_back(*dot);
				if(!coord && dot->getMeanVel() < quantille)
					dot->setType(Type::D);
				else if(!coord && dot->getMeanVel() <= quantille)
					dot->setType(Type::I);
				delete dot;
				dot = nullptr;
			}

		

			if (dot == nullptr) {
				dot = new Dot(a);
				center = *a;
				n = 1;

			}
			n++;
		}
		else if(std::next(a) != end && !std::next(a)->isBlink) {
			dot->addLast(std::next(a));
			dot->setType(Type::B);
			vec.push_back(*dot);
			
			dot = new Dot(std::next(a));
			center = *std::next(a);
			n = 1;
		}
			
	}
	return vec;
}


std::vector<Dot> Person::unite(std::vector<Dot> vec)
{
	Type prevType = vec.cbegin()->getType();
	auto prevDot = vec.cbegin();
	std::vector<Dot> newVec;
	auto gEnd = vec.cbegin()->end;
	auto gStart = vec.cbegin()->start;
	for (auto a = prevDot + 1, end = vec.cend(); a != end; ++a) {
		if (a->getType() == prevType){
			
			gEnd = a->end;
			if(std::next(a) == end)
				newVec.push_back(Dot(gStart, a->end, prevType));
		}
		else {
			newVec.push_back(Dot(gStart, gEnd, prevType));
			gStart = a->start;
			gEnd = a->end;
		}
		prevType = a->getType();
		prevDot = a;
		
	}

	return newVec;
}



std::vector<double> Person::preprocessing(std::vector<double> x){ //переделать с точки зрения структуры
	std::vector<double> v;
	int size = x.size();
	if (size%2 == 1){
		x.pop_back();
		size --;
	}

	int mu = 0;
	for (int i=1; i<(size -1)/2; i++){
		mu += i*i;
	}
	mu = 2*mu;

	for (int i=0; i<size; i++){
		v[i] = ((i-(size -1)/2)/mu)*x[i];
	}
	return v;
}
Point Person::getNewCenter(Point old, int oldSize, Point el)
{
	Point point;
	point.x = (old.x * oldSize + el.x) / (oldSize + 1);
	point.y = (old.y * oldSize + el.y) / (oldSize + 1);
	return point;
}

void Person::getQuantile(std::vector<Dot> vec)
{
	vector<double> velD;
	for ( auto dot : vec ) {
		if (dot.getType() == Type::D) {
			for (auto a = dot.start, end = dot.end; a != end; ++a)
				velD.push_back(Statistics::getAbsolute(a->velX, a->velY));
		}
	}
	std::sort(velD.begin(), velD.end());
	velD.resize((velD.size() * 95)/ 100);

	quantille = velD.back();
}




void Person::getStatistics() {

}





