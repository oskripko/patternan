//
// Created by tt on 8/15/16.
//

#ifndef PATTERNAN_STATISTICS_H
#define PATTERNAN_STATISTICS_H

#include <vector>
#include "Dot.h"

class Statistics {
public:
	static double getAbsolute(double x, double y);
	static double getMeanVel(std::vector<double>::const_iterator start, std::vector<double>::const_iterator end);

};


#endif //PATTERNAN_STATISTICS_H
