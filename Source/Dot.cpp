﻿//
// Created by tt on 8/15/16.
//

#include "Dot.h"
#include "Person.h"


const float NU = 1;
const int TYPE = 4;
const int TAU = 12;

Dot::Dot(std::vector<Point>::const_iterator element) {
	start = element;
	if (element->isBlink)
		type = Type::B;
	else
		type = Type::I;
}

Dot::Dot(std::vector<Point>::const_iterator first, std::vector<Point>::const_iterator last, Type _type)
{
	start = first;
	type = _type;
	addLast(last, _type);
}
	

Dot::~Dot() {

}

//void Dot::makeStatistics() {
//
//}


void Dot::addLast(std::vector<Point>::const_iterator element, Type _type)
{
	end = element;
	if (_type == Type::Z && end - start > TYPE)
		type = Type::D;
	size = end - start;
	duration = size; //Исправить после получения настоящей длительности
	//meanVel = Statistics::getMeanVel();
	double sum = 0;
	for (auto a = start; a != end; ++a) {
		sum += Statistics::getAbsolute(a->velX, a->velY);
	}
	meanVel = sum / size;
}

bool Dot::diffIsLess(Point newCenter, std::vector<Point>::const_iterator current, bool coord, double quantille)
{
	if (coord) {
		for (auto a = start, end = current; a != end; ++a) {
			double offsetX = a->x - newCenter.x;
			double offsetY = a->y - newCenter.y;
			double diff = sqrt(offsetX * offsetX + offsetY * offsetY);
			if (diff > NU)
				return false;
		}
	}
	else
	{
		for (auto a = start, end = current; a != end; ++a) {
			if (Statistics::getAbsolute(a->velX, a->velY) > quantille)
				return false;
		}
	}


	return true;
}


Type Dot::getType() const
{
	return type;
}

void Dot::setType(Type _type)
{
	type = _type;
}

void Dot::checkType(double quantille)
{
	if (type == Type::I)
		if (size < TAU && meanVel < quantille)
			type = Type::D;
}

double Dot::getMeanVel()
{
	return meanVel;
}

int Dot::getDuration()
{
	return duration;
}









