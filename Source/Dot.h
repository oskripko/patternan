//
// Created by tt on 8/15/16.
//

#ifndef PATTERNAN_DOT_H
#define PATTERNAN_DOT_H

#include "Statistics.h"
#include <vector>
#include <iostream>

#include <map>
enum Type {
	D = 0,
	I,
	B,
	Z
};

//std::ostream& operator<<(std::ostream& out, const Type value) {
//	static std::map<Type, std::string > strings;
//	if (strings.size() == 0) {
//#define INSERT_ELEMENT(p) strings[p] = #p
//		INSERT_ELEMENT(D);
//		INSERT_ELEMENT(I);
//		INSERT_ELEMENT(B);
//#undef INSERT_ELEMENT
//	}
//
//	return out << strings[value];
//}

struct Point {
	double x;
	double y;
	double velX;
	double velY;
	bool isBlink;
};

struct PersonProp
{
	std::vector<Point> coord;
	std::vector<Point> blink;//??
};

class Dot {
public:
    Dot(std::vector<Point>::const_iterator element);
	Dot(std::vector<Point>::const_iterator first, std::vector<Point>::const_iterator last, Type _type);
    //Узнать, удалится ли сегмент при удалении векторов сегментов.
    ~Dot();
    //void makeStatistics();
	void addLast(std::vector<Point>::const_iterator element, Type _type = Type::Z);
	bool diffIsLess(Point newCenter, std::vector<Point>::const_iterator current, bool coord, double quantille);
	Type getType() const;
	void setType(Type _type);
	void checkType(double quantille);
	double getMeanVel();
	int getDuration();

	std::vector<Point>::const_iterator start;
	std::vector<Point>::const_iterator end;
	int size;
	

	
private:
    std::vector<std::pair<double,double>> *dot;

	//1 - D, 0 - I
	Type type;
	double meanVel;
	int duration;
};


#endif //PATTERNAN_DOT_H
