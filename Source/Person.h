//
// Created by tt on 8/15/16.
//

#ifndef PATTERNAN_PERSON_H
#define PATTERNAN_PERSON_H

#include "Dot.h"


//Все данные относительно человека. Подгружаются по одному для каждого объекта (человека).


class Person {

public:
    Person(PersonProp *prop);
    //Происходит удаление всех полей (сегментов) и запись данных по этому человеку в файл с названием "id_человека".
    ~Person();
    //Существует два этапа работы. Первый выполняется, если переменная ***Seg пуста, второй в обратном случае. В обоих этапах создается оба типа сегмента.
    //output - Запись данных в соответствующую переменную.
	std::vector<Dot> makeSegments();
    //Собирает необходимую статистику по данному человеку.
    void getStatistics();
	std::vector<double> preprocessing(std::vector<double> x);
	Point getNewCenter(Point old, int oldSize, Point el);
	void getQuantile(std::vector<Dot> vec);
	std::vector<Dot> unite(std::vector<Dot> vec = {});

	

private:
	PersonProp *property;
	
    std::vector<Dot> coordFstSeg;
    std::vector<Dot> coordSndSeg;
	std::vector<Dot> coordTrdSeg;
    std::vector<Dot> velFstSeg;
    std::vector<Dot> velSndSeg;
	double quantille;
	std::vector<Dot> initialSeg(bool coord);
};

#endif //PATTERNAN_PERSON_H
