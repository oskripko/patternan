//
// Created by tt on 8/15/16.
//


#include <QtCore/QDir>
#include <QQuickView>
#include "driveWorker.h"


#include <iostream>
#include <string>
#include <fstream>

QStringList driveWorker::getFileNames() {
    //Создание properties для хранения таких вещей как директория итд.
    const QString dirPath = "..";
    QDir dir(dirPath);
    return dir.entryList();
}

PersonProp driveWorker::readCSV()
{
	Point point;
	std::ifstream ifile("D:\\PatternAn\\build\\Debug\\simple.csv", std::ifstream::in);
	std::string x, y, velX, velY, zero;
	if (!ifile.is_open()) {
		std::cout << "There was a problem opening the input file!\n";
		exit(1);//exit or do additional error checking
	}
	while (std::getline(ifile, x, ',')) {
		static int i = 0;
		++i;
		if (x == ".")
			point.x = 0;
		else
			point.x = std::stod(x);

		std::getline(ifile, y, ',');
		if (y == ".")
			point.y = 0;
		else
			point.y = std::stod(y);

		std::getline(ifile, zero, ',');
		point.isBlink = std::stoi(zero);
		if (point.isBlink) {

		}
		std::getline(ifile, velX, ',');
		if (velX == ".")
			point.velX = 0;
		else
			point.velX = std::stod(velX);

		std::getline(ifile, velY);
		if (velY == ".")
			point.velY = 0;//Read manual
		else
			point.velY = std::stod(velY);
		//if (zero == "0")
			prop->coord.push_back(point);
	}
	ifile.close();
	
	return *prop;
}

void driveWorker::writeToFile(const std::vector<Dot> &dots) {
	std::string tStr[] = { "D", "I", "B" };
	std::ofstream ofs("D:\\PatternAn\\build\\Debug\\out");
	for (auto dot : dots)
		ofs << tStr[dot.getType()] << "(" << dot.size << "," << dot.getMeanVel() << ")" << std::endl;
	ofs << dots.size() << std::endl;
}

QList<Line> PropertyModel::m_lines = {};

PeopleList::PeopleList() {
	
	driveWorker dw;
	*prop = dw.readCSV();
	person = new Person(prop);
	


	QQuickView view;
	QQmlContext *ctxt = view.rootContext();
	//ctxt->setContextProperty("data", QVariant::fromValue(dataList));

}

void PeopleList::addInitData() {
	model.addLine(Line(QString("Количество сегментов"), QString::number(segments.first.nDot), QString::number(segments.second.nDot), QString::number(segments.third.nDot), QString::number(segments.fourth.nDot), QString::number(segments.fifth.nDot)));
	model.addLine(Line(QString("Количество D"), QString::number(segments.first.nD), QString::number(segments.second.nD), QString::number(segments.third.nD), QString::number(segments.fourth.nD), QString::number(segments.fifth.nD)));
	model.addLine(Line(QString("Количество I"), QString::number(segments.first.nI), QString::number(segments.second.nI), QString::number(segments.third.nI), QString::number(segments.fourth.nI), QString::number(segments.fifth.nI)));
	model.addLine(Line(QString("Количество B"), QString::number(segments.first.nB), QString::number(segments.second.nB), QString::number(segments.third.nB), QString::number(segments.fourth.nB), QString::number(segments.fifth.nB)));
	model.addLine(Line(QString("Средняя длительность D"), QString::number(segments.first.countMeanDDur), QString::number(segments.second.countMeanDDur), QString::number(segments.third.countMeanDDur), QString::number(segments.fourth.countMeanDDur), QString::number(segments.fifth.countMeanDDur)));
	model.addLine(Line(QString("Средняя длительность I"), QString::number(segments.first.countMeanIDur), QString::number(segments.second.countMeanIDur), QString::number(segments.third.countMeanIDur), QString::number(segments.fourth.countMeanIDur), QString::number(segments.fifth.countMeanIDur)));
	model.addLine(Line(QString("Средняя длительность B"), QString::number(segments.first.countMeanBDur), QString::number(segments.second.countMeanBDur), QString::number(segments.third.countMeanBDur), QString::number(segments.fourth.countMeanBDur), QString::number(segments.fifth.countMeanBDur)));
	model.addLine(Line(QString("Частота B"), QString::number(segments.first.frequencyB), QString::number(segments.second.frequencyB), QString::number(segments.third.frequencyB), QString::number(segments.fourth.frequencyB), QString::number(segments.fifth.frequencyB)));
	model.addLine(Line(QString("Частота D"), QString::number(segments.first.frequencyD), QString::number(segments.second.frequencyD), QString::number(segments.third.frequencyD), QString::number(segments.fourth.frequencyD), QString::number(segments.fifth.frequencyD)));
	model.addLine(Line(QString("Частота I"), QString::number(segments.first.frequencyI), QString::number(segments.second.frequencyI), QString::number(segments.third.frequencyI), QString::number(segments.fourth.frequencyI), QString::number(segments.fifth.frequencyI)));
	model.addLine(Line(QString("Средний интервал D"), QString::number(segments.first.meanIntervalBetwD), QString::number(segments.second.meanIntervalBetwD), QString::number(segments.third.meanIntervalBetwD), QString::number(segments.fourth.meanIntervalBetwD), QString::number(segments.fifth.meanIntervalBetwD)));
	model.addLine(Line(QString("Средний интервал B"), QString::number(segments.first.meanIntervalBetwB), QString::number(segments.second.meanIntervalBetwB), QString::number(segments.third.meanIntervalBetwB), QString::number(segments.fourth.meanIntervalBetwB), QString::number(segments.fifth.meanIntervalBetwB)));
	model.addLine(Line(QString("Средний интервал I"), QString::number(segments.first.meanIntervalBetwI), QString::number(segments.second.meanIntervalBetwI), QString::number(segments.third.meanIntervalBetwI), QString::number(segments.fourth.meanIntervalBetwI), QString::number(segments.fifth.meanIntervalBetwI)));

}
//PeopleList::PeopleList(int nDot, int nD, int nI):m_nDot(nDot), m_nD(nD), m_nI(nI)
//{
//
//}
//SLOT
void PeopleList::gedDataFromVec(std::vector<Dot> vec) {
	switch (segNumber)
	{
	case 0:
		segments.first = setAllValues(vec, segments.first);
		break;
	case 1:
		segments.second = setAllValues(vec, segments.second);
		break;
	case 2:
		segments.third = setAllValues(vec, segments.third);
		break;
	case 3:
		segments.fourth = setAllValues(vec, segments.fourth);
		break;
	case 4:
		segments.fifth = setAllValues(vec, segments.fifth);
		break;
	default:
		break;
	}

	model.setLineValue(0, QString::number(segments.first.nDot), QString::number(segments.second.nDot), QString::number(segments.third.nDot), QString::number(segments.fourth.nDot), QString::number(segments.fifth.nDot));
	model.setLineValue(1, QString::number(segments.first.nD), QString::number(segments.second.nD), QString::number(segments.third.nD), QString::number(segments.fourth.nD), QString::number(segments.fifth.nD));
	model.setLineValue(2, QString::number(segments.first.nI), QString::number(segments.second.nI), QString::number(segments.third.nI), QString::number(segments.fourth.nI), QString::number(segments.fifth.nI));
	model.setLineValue(3, QString::number(segments.first.nB), QString::number(segments.second.nB), QString::number(segments.third.nB), QString::number(segments.fourth.nB), QString::number(segments.fifth.nB));
	model.setLineValue(4, QString::number(segments.first.countMeanDDur), QString::number(segments.second.countMeanDDur), QString::number(segments.third.countMeanDDur), QString::number(segments.fourth.countMeanDDur), QString::number(segments.fifth.countMeanDDur));
	model.setLineValue(5, QString::number(segments.first.countMeanIDur), QString::number(segments.second.countMeanIDur), QString::number(segments.third.countMeanIDur), QString::number(segments.fourth.countMeanIDur), QString::number(segments.fifth.countMeanIDur));
	model.setLineValue(6, QString::number(segments.first.countMeanBDur), QString::number(segments.second.countMeanBDur), QString::number(segments.third.countMeanBDur), QString::number(segments.fourth.countMeanBDur), QString::number(segments.fifth.countMeanBDur));
	model.setLineValue(7, QString::number(segments.first.frequencyB), QString::number(segments.second.frequencyB), QString::number(segments.third.frequencyB), QString::number(segments.fourth.frequencyB), QString::number(segments.fifth.frequencyB));
	model.setLineValue(8, QString::number(segments.first.frequencyD), QString::number(segments.second.frequencyD), QString::number(segments.third.frequencyD), QString::number(segments.fourth.frequencyD), QString::number(segments.fifth.frequencyD));
	model.setLineValue(9, QString::number(segments.first.frequencyI), QString::number(segments.second.frequencyI), QString::number(segments.third.frequencyI), QString::number(segments.fourth.frequencyI), QString::number(segments.fifth.frequencyI));
	model.setLineValue(10, QString::number(segments.first.meanIntervalBetwD), QString::number(segments.second.meanIntervalBetwD), QString::number(segments.third.meanIntervalBetwD), QString::number(segments.fourth.meanIntervalBetwD), QString::number(segments.fifth.meanIntervalBetwD));
	model.setLineValue(11, QString::number(segments.first.meanIntervalBetwB), QString::number(segments.second.meanIntervalBetwB), QString::number(segments.third.meanIntervalBetwB), QString::number(segments.fourth.meanIntervalBetwB), QString::number(segments.fifth.meanIntervalBetwB));
	model.setLineValue(12, QString::number(segments.first.meanIntervalBetwI), QString::number(segments.second.meanIntervalBetwI), QString::number(segments.third.meanIntervalBetwI), QString::number(segments.fourth.meanIntervalBetwI), QString::number(segments.fifth.meanIntervalBetwI));

	//dotProperties.append(new PeopleList(m_nDot, 0, 0));
	
}



Q_INVOKABLE QString PeopleList::makeSegments()
{
	QObject::connect(this, SIGNAL(vectorReady(std::vector<Dot>)), this, SLOT(gedDataFromVec(std::vector<Dot>)));
	std::vector<Dot> vec;
	vec = person->makeSegments();
	gedDataFromVec(vec);
	segNumber++;
	driveWorker::writeToFile(vec);
	
	return Q_INVOKABLE QString("Ready");//Сюда вывод строки с результатом
}


Property PeopleList::setAllValues(std::vector<Dot> vec, Property segs)
{
	segs.nDot = vec.size();
	int durI = 0, durD = 0, durB = 0, cI = 0, cD = 0, cB = 0, nI = 0, nB = 0, nD = 0;
	for (auto dot : vec) {
		if (dot.getType() == Type::I) {
			segs.nI++;
			durI += dot.getDuration();
			nI += cI;
			cI = 0;
		}
		else if (dot.getType() == Type::D) {
			segs.nD++;
			durD += dot.getDuration();
			nD += cD;
			cD = 0;
		}
		else if (dot.getType() == Type::B) {
			segs.nB++;
			durB += dot.getDuration();
			nB += cB;
			cB = 0;//WTF?
		}
	}
	if (segs.nD != 0) {
		segs.countMeanDDur = durD / (double)segs.nD;
		segs.frequencyD = segs.nD / (double)segs.nDot;
		segs.meanIntervalBetwD = segs.nD / (double)(segs.nD - 1);
	}
	if (segs.nI != 0) {
		segs.countMeanIDur = durI / (double)segs.nI;
		segs.frequencyI = segs.nI / (double)segs.nDot;
		segs.meanIntervalBetwI = segs.nI / (double)(segs.nI - 1);
	}
	if (segs.nB != 0) {
		segs.countMeanBDur = durB / (double)segs.nB;
		segs.frequencyB = segs.nB / (double)segs.nDot;
		segs.meanIntervalBetwB = segs.nB / (double)(segs.nB - 1);
	}

	return segs;
	
}

QString Line::name() const {
	return m_name;
}

QString Line::first() const {
	return m_first;
}

QString Line::second() const {
	return m_second;
}

QString Line::third() const {
	return m_third;
}

QString Line::fourth() const {
	return m_fourth;
}

QString Line::fifth() const {
	return m_fifth;
}

PropertyModel::PropertyModel(QObject *parent) : QAbstractTableModel(parent) {}

void PropertyModel::addLine(const Line & line)
{
	beginInsertRows(QModelIndex(), rowCount(), rowCount());
	m_lines << line;
	endInsertRows();
	layoutChanged();
}



//int PropertyModel::rowCount(const QModelIndex & parent) const
//{
//	return 15;
//}

QVariant PropertyModel::data(const QModelIndex & index, int role) const
{
	if (role == 0) {
		Line line = m_lines.at(index.row());
		return line.name();
	}
	if (role == 1) {
		Line line = m_lines.at(index.row());
		return line.first();
	}
	if (role == 2) {
		Line line = m_lines.at(index.row());
		return line.second();
	}
	if (role == 3) {
		Line line = m_lines.at(index.row());
		return line.third();
	}
	if (role == 4) {
		Line line = m_lines.at(index.row());
		return line.fourth();
	}
	if (role == 5) {
		Line line = m_lines.at(index.row());
		return line.fifth();
	}
	return QVariant();
}

QHash<int, QByteArray> PropertyModel::roleNames() const
{
	QHash<int, QByteArray> roles;
	roles[0] = "Prop";
	roles[1] = "XY";
	roles[2] = "Aggreg";
	roles[3] = "Quantille based";
	roles[4] = "V segmentation";
	roles[5] = "VAggreg";
	return roles;
}

//QVariant PropertyModel::headerData(int section, Qt::Orientation orientation, int role) const
//{
//	if (role == Qt::DisplayRole)
//	{
//		if (orientation == Qt::Horizontal) {
//			switch (section)
//			{
//			case 0:
//				return QString("name");
//			case 1:
//				return QString("second");
//			case 2:
//				return QString("third");
//			}
//		}
//	}
//	return QVariant();
//}
